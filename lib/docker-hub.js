const fetch = require("node-fetch");

function getToken(repository) {
  var url = `https://auth.docker.io/token?service=registry.docker.io&scope=repository:${repository}:pull`;

  return fetch(url)
    .then(res => checkStatus(res, url))
    .then(res => res.json())
    .then(({ token }) => token);
}

function getTags(repository, token) {
  var url = `https://registry-1.docker.io/v2/${repository}/tags/list`;

  return fetch(url, {
    headers: new fetch.Headers({
      "Authorization": `Bearer ${token}`
    })
  })
    .then(res => checkStatus(res, url))
    .then(res => res.json())
    .then(({ tags }) => tags);
}

function checkStatus(res, url) {
  if (res.ok) {
    return res;
  } else {
    throw `Unable to list docker tags at ${url}: ${res.status} ${res.statusText}`;
  }
}

function ensureTagIsPushed(repository, tag) {
  return getToken(repository)
    .then(token => getTags(repository, token))
    .then(tags => {
      if (tags.indexOf(tag) === -1) {
        var name = repository + ":" + tag;
        throw "Unable to find " + name + " on Docker Hub. Please run `docker push " + name + "`.";
      }
      return tags;
    });
}

module.exports = {
  ensureTagIsPushed: ensureTagIsPushed
};
