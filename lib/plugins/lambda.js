const exec = require("../exec");
const path = require("path");
const util = require("util");

const tmp = require("tmp");
const tmpName = util.promisify(tmp.tmpName);

function zipLambda(args) {
  if (!args.stack.lambda) {
    return args;
  }

  var cwd = process.cwd();
  var folderToZip = path.resolve(path.dirname(args.stackPath), args.stack.lambda.folder);
  process.chdir(folderToZip);

  return tmpName()
    .then(localZipPath => {
      localZipPath += ".zip";
      var command = ["zip", "-r", localZipPath, ".", "-i", "*"];
      // console.log(command.join(" "));

      if (exec.forExitCode.apply(this, command)) {
        process.chdir(cwd);
        return localZipPath;
      } else {
        throw "zip failed";
      }
    }).then((localZipPath) => {
      var zipFile = args.stack.stackName + "-" + args.commit + ".zip";
      var s3Path = "s3://" + args.stack.lambda.bucket + "/" + zipFile;
      var command = [
        "aws", "s3",
        "cp", localZipPath, s3Path,
        "--profile", args.stack.awsCliProfile
      ];
      // console.log(command.join(" "));

      if (exec.forExitCode.apply(this, command)) {
        return zipFile;
      } else {
        throw `Upload of lambda code failed. Command was: "${command.join(" ")}"`;
      }
    }).then(zipFile => {
      args.stack.parameters[args.stack.lambda.parameter] = zipFile;
      return args;
    });
}

module.exports = {
  beforeDeploy: zipLambda
};
