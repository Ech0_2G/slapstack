const git = require("../git");

function ensureCleanWorkingDirectory(args) {
  if (git.status() !== "") {
    throw "Working directory must be clean! Stash or commit your work.";
  }
  return args;
}

module.exports = {
  configuration: ensureCleanWorkingDirectory
};
