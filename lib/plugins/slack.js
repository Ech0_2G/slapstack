const slack = require("../slack");

function notifySlack(args) {
  if (!args.stack.slack) {
    return args;
  }

  var webhookUrl = args.stack.slack.webhookUrl;
  var channel = args.stack.slack.channel;

  var commitUrl = "";
  if (args.commit) {
    commitUrl = "of commit " + args.commit;
  }

  var progressUrl = "https://console.aws.amazon.com/cloudformation/home?region=us-east-1#/stacks?filter=active";
  var message = `${process.env.USER} has started deployment ${commitUrl} for "${args.stack.stackName}" follow progress at ${progressUrl}`;

  return slack(webhookUrl, channel, message, "construction")
    .then(() => args);
}

module.exports = {
  beforeDeploy: notifySlack
};
