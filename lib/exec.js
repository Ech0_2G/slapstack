const childProcess = require("child_process");

function execForExitCode(command) {
  var args = Array.prototype.slice.call(arguments, 1);
  var info = childProcess.spawnSync(command, args, { stdio: "inherit" });
  return info.status === 0;
}

function execForStdout(command) {
  var args = Array.prototype.slice.call(arguments, 1);
  var info = childProcess.spawnSync(command, args, { encoding: "utf-8" });
  return info.stdout;
}

module.exports = {
  forExitCode: execForExitCode,
  forStdout: execForStdout
};
