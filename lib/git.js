const exec = require("./exec");

function currentCommit() {
  return exec.forStdout("git", "rev-parse", "--short", "HEAD").trim();
}

function status() {
  return exec.forStdout("git", "status", "--porcelain");
}

module.exports = {
  currentCommit: currentCommit,
  status: status
};
