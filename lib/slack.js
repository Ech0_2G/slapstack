const fetch = require("node-fetch");
const querystring = require("querystring");

function slackMessage(channel, message, emoji) {
  var payload = {
    channel: channel,
    username: "CloudFormation",
    text: message,
    icon_emoji: ":" + emoji + ":" // eslint-disable-line camelcase
  };
  return querystring.stringify({
    payload: JSON.stringify(payload)
  });
}

function slack(webhookUrl, channel, message, emoji) {
  return fetch(webhookUrl, {
    method: "POST",
    headers: {
      "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
    },
    body: slackMessage(channel, message, emoji)
  });
}

module.exports = slack;
