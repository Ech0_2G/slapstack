const exec = require("./exec");
const drc = require("./docker-registry-client");

function getTags(profile, region, repository, tag) {
  var token = exec.forStdout(
    "aws", "ecr", "get-authorization-token",
    "--profile", profile,
    "--region", region,
    "--output", "text",
    "--query", "authorizationData[].authorizationToken"
  ).trim();

  var url = new URL(`https://${repository}`);
  return drc.getTags(url.hostname, url.pathname.substr(1), tag, token);
}

function ensureTagIsPushed(profile, region, repository, tag) {
  return getTags(profile, region, repository, tag).then((res) => {
    if (!res) {
      var name = repository + ":" + tag;
      throw "Unable to find " + name + " on AWS ECR. Please run `docker push " + name + "`.";
    }
    return res;
  });
}

module.exports = {
  ensureTagIsPushed: ensureTagIsPushed
};
