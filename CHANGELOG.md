# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.5.0] - 2020-09-19
### Changed
- Updated dependencies
- Implement my own docker registry client because the one we were using is
  abandoned and has security vulnerabilities. It works for public repos, but
  might not work for private ones. I don't have any private ones to test with.

## [0.4.0] - 2019-10-01
### Added
- Support for specifying stack policies.

## [0.3.0] - 2019-09-25
### Added
- Support for specifying template capabilities like CAPABILITY_IAM.
### Fixed
- Don't silently ignore errors importing stacks and parsing them.

## [0.2.2] - 2019-09-09
### Fixed
- Remove extra console.log()

## [0.2.1] - 2019-09-09
### Fixed
- Accidentally commented out git status error

## [0.2.0] - 2019-09-09
### Added
- Support for checking AWS ECR repositories that Docker images are pushed

## [0.1.2] - 2018-12-03
### Fixed
- Fix crash when stack file had no parameters

## [0.1.1] - 2018-11-15
### Changed
- Require `dockerHub.repository` to be specified for the Docker plugin to run.
  This allows you to specify partial settings in a common import file without
  triggering the plugin to run unless a repository is given.

## [0.1.0] - 2018-11-15
### Fixed
- Specify aws-cli profile when uploading lambda code
### Changed
- Moved slack settings from `secrets` key into `slack` key and documented them.
- Moved & renamed all Docker Hub settings key into `dockerHub` key and documented them.

## [0.0.6] - 2018-11-12
### Added
- Documentation and examples in README.md
### Fixed
- Remove reference to environment in Slack message

## [0.0.5] - 2018-10-31
### Changed
- Fixed bug in merging imports

## [0.0.4] - 2018-10-31
### Added
- Stack files now have an `imports` list of files that get merged into the stack
### Changed
- Commit to deploy is now specified in a command-line argument rather than
  implicitly queried from git.
### Removed
- Support for environments was removed. Instead of specifying multiple environments in one stack file, just use one file per environment.
- Support for defaults files and secrets files were removed. Use the new
  `imports` list instead.

## [0.0.3] - 2018-10-24
### Changed
- Updated dependencies
- Allow defaults to be specified in a `slapstack-defaults.yml` with the stack's
  settings overriding the defaults.
- Read awsCliProfile from the stack's settings rather than the AWS_PROFILE
  environment variable.
- `secretsPath` is resolved relative to the stack file rather than
  `package.json`

## [0.0.2] - 2018-06-08
### Changed
- Include full repository URL so it shows up on NPM correctly

## [0.0.1] - 2018-06-08
### Added
- Changelog
- Configurable secrets file

### Changed
- Move repo from github to gitlab
- Refactor internals to support future plugin system

## [0.0.0] - 2018-05-22
### Added
- Initial release
